package movementOO;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Bullet
{
	private double x, y;
	private int radius;
	private double speed_shoot;
	private boolean shoot = false;
	
	public void SetPos(double x, double y)
	{
		this.x = x;
		this.y = y;
	}

	public Bullet(int x, int y)
	{
		super();
		this.x = x;
		this.y = y;
		this.radius = 15;
		this.speed_shoot = 0.15;
	}

	public boolean isShoot()
	{
		return shoot;
	}

	public void update(GameContainer container, int delta)
	{       
        if(shoot)
        {
			this.speed_shoot += ((Math.exp(0.01)-1));
			this.y -= delta*this.speed_shoot;
        }

		if(this.y <= -20)
		{
			shoot = false;
		}
	}
	
	public void setShoot(boolean shoot)
	{
		this.shoot = shoot;
		this.speed_shoot = 0.15;
	}

	public void render(Graphics g)
	{
		if (shoot == true)
		{
			g.setColor(Color.magenta);
		}
		else
		{
			g.setColor(Color.transparent);
		}
		
		g.fillRoundRect((int)this.x, (int)this.y, this.radius, this.radius, 90);
	}
}

