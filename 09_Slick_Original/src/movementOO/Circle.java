package movementOO;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class Circle implements Actor
{
	private int x, y;
	private int radius;
	private int movement;

	public Circle(int x, int y, int radius)
	{
		super();
		this.x = x;
		this.y = y;
		this.radius = radius;
	}

	public void update(int delta)
	{

		if(movement==0)
		{
			this.x++;
			if(x>=700)
			{
				movement=1;
			}
		}
		if(movement==1)
		{
			this.x--;
			if(x<=0)
			{
				movement=0;
			}
		}
	}

	public void render(Graphics g)
	{
		g.setColor(Color.green);
		g.drawRoundRect(this.x, this.y, this.radius, this.radius, 90);
	}

}
