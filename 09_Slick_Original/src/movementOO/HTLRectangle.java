package movementOO;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class HTLRectangle implements Actor
{
	private int x, y;
	private int width, height;
	private int angle;

	public HTLRectangle(int x, int y, int width, int height)
	{
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public void update(int delta)
	{
		this.angle++;
		this.x = (int)(Math.cos(this.angle * Math.PI/180)*180 + 200);
		this.y = (int)(Math.sin(this.angle * Math.PI/180)*180 + 300);
	}

	public void render(Graphics g)
	{
		g.setColor(Color.blue);
		g.drawRect(this.x, this.y, this.width, this.height);
	}
}
