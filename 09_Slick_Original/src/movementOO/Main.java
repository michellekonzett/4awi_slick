package movementOO;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class Main extends BasicGame
{
	private List <Actor> actor;
	
	private double CannonBall_x;
	private double CannonBall_y;
	
	HTLRectangle Rectangle = new HTLRectangle(10, 20, 100, 100);
	Circle Circle = new Circle(300, 300, 100);
	Oval Oval = new Oval(300, 200, 100, 200);
	Snowflake SmallSnow = new Snowflake(Snowflake.size.small);
	Snowflake MedSnow = new Snowflake(Snowflake.size.medium);
	Snowflake BigSnow = new Snowflake(Snowflake.size.big);
	Shootingstar Shootingstar = new Shootingstar();
	Starship Starship = new Starship(300, 500);
	Bullet CannonBall = new Bullet((int)CannonBall_x, (int)CannonBall_y);
	
	public Main()
	{
		super("First Game");
	}

	@Override
	public void render(GameContainer gameContainer, Graphics graphics) throws SlickException
	{
		for (Actor actor : actor)
		{
			actor.render(graphics);
		}
		
		Starship.render(graphics);
		CannonBall.render(graphics);
	}

	@Override
	public void init(GameContainer gameContainer) throws SlickException
	{
		actor = new ArrayList<>();
		
		actor.add(Rectangle);
		actor.add(Circle);
		actor.add(Oval);
		actor.add(Shootingstar);

		for(int i=0; i<=50; i++)
		{
			actor.add(new Snowflake(Snowflake.size.small));
			actor.add(new Snowflake(Snowflake.size.medium));
			actor.add(new Snowflake(Snowflake.size.big));
		}
	}

	@Override
	public void update(GameContainer container, int delta) throws SlickException
	{
		for (Actor actor : actor)
		{
			actor.update(delta);
		}
		
		if(container.getInput().isKeyDown(Input.KEY_SPACE) && !CannonBall.isShoot())
		{
			CannonBall.SetPos(Starship.getX() + 42.5, Starship.getY() + 25);
			CannonBall.setShoot(true);
		}
		
		Starship.update(container, delta);
		CannonBall.update(container, delta);
		
		CannonBall_x = Starship.getX();
		CannonBall_y = Starship.getY();
	}

	public static void main(String[] argv)
	{
		try
		{
			AppGameContainer container = new AppGameContainer(new Main());
			container.setDisplayMode(800, 600, false);
			container.start();
		} 
		catch (SlickException e)
		{
			e.printStackTrace();
		}
	}
}