package movementOO;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class Oval implements Actor
{
	private int x, y;
	private int width, height;

	public Oval(int x, int y, int width, int height)
	{
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public void update(int delta)
	{
		this.y++;
		if(y>=600)
		{
			y=-200;
		}
	}
	
	public void render(Graphics g)
	{
		g.setColor(Color.pink);
		g.drawOval(this.x, this.y, this.width, this.height);
	}
}
