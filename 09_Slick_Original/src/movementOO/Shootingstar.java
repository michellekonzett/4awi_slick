package movementOO;

import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class Shootingstar implements Actor
{	
	private double x, y;
	private int radius;
	private double speed_x, speed_y;
	
	Random rnd = new Random();
	
	public Shootingstar()
	{
		super();
		this.x = rnd.nextInt(100)+800;
		this.y = rnd.nextInt(100)+50;
		this.radius = rnd.nextInt(10)+10;
		this.speed_x = 0.1;
		this.speed_y = 0.005;
	}
	
	int deltatime = 0;
	public void update(int delta)
	{
		this.speed_x += 0.005;
		this.x -= delta*this.speed_x;
		this.speed_y += ((Math.exp(0.001)-1));
		this.y += delta*this.speed_y;
		
		deltatime += delta;
		if(deltatime >= 200)
		{
			this.radius -= 1.5;
			deltatime = 0;
		}
		
		if(this.x<= -500)
		{
			this.x = rnd.nextInt(100)+800;
			this.y = rnd.nextInt(100)+50;
			this.radius = rnd.nextInt(20)+10;
			this.speed_x = 0.05;
			this.speed_y = 0.005;
		}
	}

	public void render(Graphics g)
	{	
		g.setColor(Color.yellow);
		g.fillRoundRect((int)this.x, (int)this.y, this.radius, this.radius, 90);
	}
}
