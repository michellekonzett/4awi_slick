package movementOO;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import java.util.Random;

public class Snowflake implements Actor
{
	public enum size
	{
		small, medium, big
	}
	
	private double x, y;
	private int width, height;
	private double speed;
	private size size;

	Random rnd = new Random();
	
	public Snowflake(size size)
	{
		super();
		this.x = rnd.nextInt(800);
		this.y = rnd.nextInt(600)-600;
		this.size = size;
	}

	public void update(int delta)
	{
		this.y += delta*this.speed;
	}

	public void render(Graphics g)
	{
		if(this.size == size.small)
		{
			this.width = 10;
			this.height = 10;
			this.speed = 0.2;
		}
		if(this.size == size.medium)
		{
			this.width = 20;
			this.height = 20;
			this.speed = 0.3;
		}
		if(this.size == size.big)
		{
			this.width = 30;
			this.height = 30;
			this.speed = 0.4;
		}
		
		if(this.y>=600)
		{
			this.y= -20;
			this.x = rnd.nextInt(800);
		}
		
		g.setColor(Color.white);
		g.fillRoundRect((int)this.x, (int)this.y, this.width, this.height, 90);
	}
}
