package movementOO;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

public class Starship
{
	private double x, y;
	private int width, height;
	private boolean moveleft = false;
	private boolean moveright = false;
	private boolean moveup = false;
	private boolean movedown = false;

	public Starship(int x, int y)
	{
		super();
		this.x = x;
		this.y = y;
		this.width = 100;
		this.height = 50;
	}
	
	public double getX()
	{
		return x;
	}
	public void setX(double x)
	{
		this.x = x;
	}
	
	public double getY()
	{
		return y;
	}
	public void setY(double y)
	{
		this.y = y;
	}

	public void update(GameContainer container, int delta)
	{
        moveleft = container.getInput().isKeyDown(Input.KEY_LEFT);
        moveright = container.getInput().isKeyDown(Input.KEY_RIGHT);
        moveup = container.getInput().isKeyDown(Input.KEY_UP);
        movedown = container.getInput().isKeyDown(Input.KEY_DOWN);
  
		if(moveleft == true)
		{
			this.x--;
			if(this.x <= 0)
			{
				this.x++;
			}
		}
		if(moveright == true)
		{
			this.x++;
			if(this.x >= 700)
			{
				this.x--;
			}
		}
		if(moveup == true)
		{
			this.y--;
			if(this.y <= 0)
			{
				this.y++;
			}
		}
		if(movedown == true)
		{
			this.y++;
			if(this.y >= 550)
			{
				this.y--;
			}
		}
	}

	public void render(Graphics g)
	{
		g.setColor(Color.cyan);
		g.fillOval((int)this.x, (int)this.y, this.width, this.height);
	}
}
