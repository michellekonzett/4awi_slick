package superMeatBoy;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;

public class BurgerEnemy implements Enemy
{
	private double x, y;
	private int movement;
	private Image burgerImage;
	private int right, left, level;
	public Rectangle enemy;
	
	public BurgerEnemy() throws SlickException
	{
		super();
		enemy = new Rectangle((int)x, (int)y, 50, 50);
		burgerImage = new Image("testdata/supermeatboy/burger/level2.png");
		setLevel(1);
	}

	public void setLevel(int level) throws SlickException
	{
		this.level = level;
		if (level == 2)
		{
			enemy.setWidth(33);
			enemy.setHeight(33);
			burgerImage = new Image("testdata/supermeatboy/burger/level2.png");
			setEnemyValues(800, 727, 1300, 615);
		}
		if (level == 4)
		{
			enemy.setWidth(75);
			enemy.setHeight(35);
        	burgerImage = new Image("testdata/supermeatboy/burger/level4.png");
			setEnemyValues(300, 45, 1420, 150);
		}
		if (level == 6)
		{
			enemy.setWidth(40);
			enemy.setHeight(36);
        	burgerImage = new Image("testdata/supermeatboy/burger/level6.png");
			setEnemyValues(750, 364, 815, 695);
		}
		if (level == 9)
		{
			enemy.setWidth(57);
			enemy.setHeight(45);
        	burgerImage = new Image("testdata/supermeatboy/burger/level9.png");
    		enemy.setX(25);
    		enemy.setY(400);
    		movement = 2;
		}
		if (level == 10)
		{
			enemy.setWidth(39);
			enemy.setHeight(33);
        	burgerImage = new Image("testdata/supermeatboy/burger/level10.png");
			setEnemyValues(300, 727, 1450, 100);
		}		
	}

	private void setEnemyValues(int x, int y, int right, int left)
	{
		enemy.setX(x);
		enemy.setY(y);
		this.right = right;
		this.left = left;
	}
	
	public Rectangle getRectangle()
	{
		return enemy;
	}
	
	public void update(GameContainer container, int delta) throws SlickException
	{
		if (movement == 0)
		{
			enemy.setX(enemy.getX()+0.3f);
			if (enemy.getX() >= this.right)
			{
				movement = 1;
			}
		}
		if (movement == 1)
		{
			enemy.setX(enemy.getX()-0.3f);
			if (enemy.getX() <= this.left)
			{
				movement = 0;
			}
		}
		
		if(level ==9)
		{
			if (movement == 2)
			{
				enemy.setY(enemy.getY()+0.3f);
				if (enemy.getY() >= 580)
				{
					movement = 3;
				}
			}
			if (movement == 3)
			{
				enemy.setY(enemy.getY()-0.3f);
				if (enemy.getY() <= 315)
				{
					movement = 2;
				}
			}
		}
		
        if (level == 1 |level == 3 |level == 5 | level == 7 | level == 8)
        {
        	enemy.setX(-100);
        }
	}

	public void render(Graphics g)
	{
		burgerImage.draw((float) enemy.getX(), (float) enemy.getY(), enemy.getWidth(), enemy.getHeight());
	}
}
