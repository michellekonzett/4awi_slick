package superMeatBoy;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;

public class DoctorFetus implements Enemy
{
	private double x, y;
	private Image doctorFetusImage;
	private int level;
	public Rectangle enemy;
	
	public DoctorFetus() throws SlickException
	{
		super();
		enemy  = new Rectangle((int)x, (int)y, 42, 55);
		doctorFetusImage = new Image("testdata/supermeatboy/doctorfetus.png");
		setLevel(1);
	}

	public void setLevel(int level) throws SlickException
	{
		this.level = level;
		if (level == 1 | level == 2 | level == 3)
		{
			setEnemyValues(1400, 705);
		}
		if (level == 4)
		{
			setEnemyValues(600, 705);
		}
		if (level == 5)
		{
			setEnemyValues(250, 705);
		}
		if (level == 6 | level == 7)
		{
			setEnemyValues(1300, 705);
		}
		if (level == 8)
		{
			setEnemyValues(1335, 545);
		}
		if (level == 9)
		{
			setEnemyValues(900, 345);
		}
		if (level == 10)
		{
			setEnemyValues(970, 345);
		}
	}

	private void setEnemyValues(int x, int y)
	{
		enemy.setX(x);
		enemy.setY(y);
	}
	
	public Rectangle getRectangle()
	{
		return enemy;
	}
	
	public void update(GameContainer container, int delta) throws SlickException
	{

	}

	public void render(Graphics g)
	{
		doctorFetusImage.draw((float) enemy.getX(), (float) enemy.getY(), enemy.getWidth(), enemy.getHeight());
	}
}
