package superMeatBoy;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;

public class DogEnemy implements Enemy
{
	private double x, y;
	private int movement = 0;
	private Image dogImage;
	private int right, left, level;
	public Rectangle enemy;

	public DogEnemy() throws SlickException
	{
		super();
		enemy = new Rectangle((int)x, (int)y, 46, 60);
		dogImage = new Image("testdata/supermeatboy/dog/level1.png");
		setLevel(1);
	}

	public void setLevel(int level) throws SlickException
	{
		this.level = level;
		if (level == 1)
		{
			enemy.setWidth(46);
			enemy.setHeight(60);
			dogImage = new Image("testdata/supermeatboy/dog/level1.png");
			setDogValues(300, 700, 545, 230);
		}
		if (level == 3)
		{
			enemy.setWidth(55);
			enemy.setHeight(70);
			dogImage = new Image("testdata/supermeatboy/dog/level3.png");
			setDogValues(700, 690, 790, 515);
		}
		if (level == 5)
		{
			enemy.setWidth(46);
			enemy.setHeight(67);
			dogImage = new Image("testdata/supermeatboy/dog/level5.png");
			setDogValues(1200, 693, 1450, 1065);
		}
		if (level == 7)
		{
			enemy.setWidth(40);
			enemy.setHeight(69);
			dogImage = new Image("testdata/supermeatboy/dog/level7.png");
			setDogValues(264, 692, 645, 0);
		}
		if (level == 8)
		{
			enemy.setWidth(35);
			enemy.setHeight(58);
			dogImage = new Image("testdata/supermeatboy/dog/level8.png");
			setDogValues(900, 702, 1455, 0);
		}
		if (level == 9)
		{
			enemy.setWidth(40);
			enemy.setHeight(69);
			dogImage = new Image("testdata/supermeatboy/dog/level9.png");
			setDogValues(1200, 561, 1455, 1115);
		}
		if (level == 10)
		{
			enemy.setWidth(46);
			enemy.setHeight(60);
			dogImage = new Image("testdata/supermeatboy/dog/level10.png");
			setDogValues(700, 580, 1190, 120);
		}
	}

	private void setDogValues(int x, int y, int right, int left)
	{
		enemy.setX(x);
		enemy.setY(y);
		this.right = right;
		this.left = left;
	}

	public Rectangle getRectangle()
	{
		return enemy;
	}
	
	public void update(GameContainer container, int delta) throws SlickException
	{
		if (movement == 0)
		{
			enemy.setX(enemy.getX()+0.3f);
			if (enemy.getX() >= this.right)
			{
				movement = 1;
			}
		}
		if (movement == 1)
		{
			enemy.setX(enemy.getX()-0.3f);
			if (enemy.getX() <= this.left)
			{
				movement = 0;
			}
		}
		
		if (level == 2 | level == 4 | level == 6)
		{
			enemy.setX(-100);
		}
	}

	public void render(Graphics g)
	{
		dogImage.draw((float) enemy.getX(), (float) enemy.getY(), enemy.getWidth(), enemy.getHeight());
	}
}
