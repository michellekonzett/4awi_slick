package superMeatBoy;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;

public class Door
{
	public Image doorImageNormal;
	public Image doorImageFinished;
	public boolean finished;
	public Rectangle door;

	public Door() throws SlickException
	{
		super();
		door = new Rectangle(25, 48, 57, 53);
		doorImageNormal = new Image("testdata/supermeatboy/door.png");
		doorImageFinished = new Image("testdata/supermeatboy/bandagegirlDoor.png");
	}

	public void update(GameContainer container, int delta) throws SlickException
	{
//		doorImage = new Image("testdata/supermeatboy/door.png");
	}

	public void render(Graphics g)
	{
		if(!finished) doorImageNormal.draw((float) door.getX(), (float) door.getY(), door.getWidth(), door.getHeight());
		else doorImageFinished.draw((float) door.getX(), (float) door.getY(), door.getWidth(), door.getHeight());
	}
}
