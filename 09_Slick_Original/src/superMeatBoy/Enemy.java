package superMeatBoy;

import org.newdawn.slick.geom.Rectangle;

public interface Enemy
{
	public Rectangle getRectangle();
}
