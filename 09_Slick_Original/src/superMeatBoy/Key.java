package superMeatBoy;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;

public class Key
{
	private double x, y;
	private Image keyImage;
	public Rectangle key;

	public Key(int x, int y) throws SlickException
	{
		super();
		this.x = x;
		this.y = y;
		key = new Rectangle((int)x, (int)y, 22, 36);
		keyImage = new Image("testdata/supermeatboy/key.png");
	}

	public void update(GameContainer container, int delta)
	{

	}

	public void render(Graphics g)
	{
		keyImage.draw((float) key.getX(), (float) key.getY(), key.getWidth(), key.getHeight());
	}
}
