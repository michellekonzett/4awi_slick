package superMeatBoy;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;

import movementOO.Actor;

public class Main extends BasicGame
{
	//design patterns
	private Meatboy meatboy;
	private DogEnemy dog;
	private BurgerEnemy burger;
	private Platform1 bottom;
	private DoctorFetus doctorFetus;
	private Door door;
	public int level;
	private boolean pause = false;
	private KeyCounter keys;
	private List <Platform1> platforms1;
	private List <Platform2> platforms2;
	private List <Key> key;
	private List <Enemy> enemy;
	private long timeStarted = 0;
	public boolean restart = false;

	public Main()
	{
		super("Supermeatboy 2.0");
	}

	@Override
	public void render(GameContainer container, Graphics g) throws SlickException
	{
		g.drawString("Level: " + level, 10, 10);
		g.drawString("Keys: " + keys.count, 100, 10);
		
		if(level == 1)
		{
			g.drawString("Oh no! Doctor Fetus kidnapped Bandage girl.", 800, 35);
			g.drawString("Collect all the keys to open the door and save her.", 800, 50);
			g.drawString("But watch out: his friends are evil too.", 800, 65);
			
			g.drawString("left: key left", 1300, 30);
			g.drawString("right: key right", 1300, 45);
			g.drawString("jump: key up", 1300, 60);
			g.drawString("restart: r", 1300, 75);
		}
		
		g.setBackground(Color.white);
		meatboy.render(g);
		
		if(pause == true && System.currentTimeMillis() - timeStarted > 2000)
		{
			level++;
			System.out.println("level: " + level);
			nextLevel(level);
			door.finished = false;
			pause = false;
		}
		
		for(Platform1 platforms:platforms1)
		{
			platforms.render(g);
		}
		for(Platform2 platforms:platforms2)
		{
			platforms.render(g);
		}
		
		for(Key key:key)
		{
			key.render(g);
		}
		
		door.render(g);
		meatboy.render(g);		
		dog.render(g);
		burger.render(g);
		doctorFetus.render(g);
		bottom.render(g);
	}

	@Override
	public void init(GameContainer container) throws SlickException
	{
		level = 1; 
		this.meatboy = new Meatboy(75, 700);
		this.dog = new DogEnemy();
		this.burger = new BurgerEnemy();
		this.doctorFetus = new DoctorFetus();
		this.door = new Door();
		this.bottom = new Platform1(0, 760, 1920, 500); //1500, 200
		
		this.enemy = new ArrayList();
		enemy.add(burger);
		enemy.add(dog);
		enemy.add(doctorFetus);
		
		this.platforms1 = new ArrayList();
		this.platforms2 = new ArrayList();
		platforms1.add(new Platform1(0, 100, 150, 15));
		platforms1.add(new Platform1(300, 250, 150, 15));
		platforms1.add(new Platform1(600, 500, 150, 15));
		platforms1.add(new Platform1(250, 640, 350, 15));
		platforms1.add(new Platform1(900, 640, 150, 15));
		platforms2.add(new Platform2(900, 640, 15, 300));
		platforms2.add(new Platform2(600, 500, 15, 600));
		
		this.key = new ArrayList();
		key.add(new Key(400, 285));
		key.add(new Key(900, 510));
		key.add(new Key(300, 700));
		key.add(new Key(625, 700));
		key.add(new Key(1000, 700));
		
		try
		{
			dog.setLevel(1);
			burger.setLevel(1);
		} catch (SlickException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		keys = new KeyCounter();
	}

	@Override
	public void update(GameContainer container, int delta) throws SlickException
	{
		restart = container.getInput().isKeyDown(Input.KEY_R);
		if(restart == true)
		{
			keys.count = 0;
			level = 1;
			nextLevel(level);
		}
		
		if(!pause)
		{
			meatboy.update(container, delta, platforms1, platforms2, enemy, key, keys, door, restart);
			dog.update(container, delta);
			burger.update(container, delta);
			door.update(container, delta);
			
			if(keys.count == 5 && meatboy.nextLevel == true)
			{
				door.finished = true;
				pause = true;
				keys.count = 0;
				meatboy.nextLevel = false;
				timeStarted = System.currentTimeMillis();
			}
			
			doctorFetus.update(container, delta);
			bottom.update(delta);
		}
	}
	
	public void nextLevel(int level)
	{	
		if(level==1)
		{
			platforms1.clear();
			platforms2.clear();
			platforms1.add(new Platform1(0, 100, 150, 15));
			platforms1.add(new Platform1(300, 250, 150, 15));
			platforms1.add(new Platform1(600, 500, 150, 15));
			platforms1.add(new Platform1(250, 640, 350, 15));
			platforms1.add(new Platform1(900, 640, 150, 15));
			platforms2.add(new Platform2(900, 640, 15, 300));
			platforms2.add(new Platform2(600, 500, 15, 600));
			
			key.clear();
			try
			{
				key.add(new Key(400, 285));
				key.add(new Key(900, 510));
				key.add(new Key(300, 700));
				key.add(new Key(625, 700));
				key.add(new Key(1000, 700));
				
				dog.setLevel(1);
				burger.setLevel(1);
				doctorFetus.setLevel(1);
			} catch (SlickException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(level==2)
		{
			platforms1.clear();
			platforms2.clear();
			platforms1.add(new Platform1(0, 100, 150, 15));
			platforms1.add(new Platform1(300, 250, 150, 15));
			platforms1.add(new Platform1(1100, 400, 150, 15));
			platforms1.add(new Platform1(600, 500, 150, 15));
			platforms1.add(new Platform1(250, 640, 150, 15));
			platforms1.add(new Platform1(900, 640, 150, 15));
			platforms2.add(new Platform2(250, 640, 15, 300));
			platforms2.add(new Platform2(600, 500, 15, 600));
			
			key.clear();
			try
			{
				key.add(new Key(1150, 210));
				key.add(new Key(400, 285));
				key.add(new Key(200, 700));
				key.add(new Key(625, 700));
				key.add(new Key(1250, 700));
				
				dog.setLevel(2);
				burger.setLevel(2);
				doctorFetus.setLevel(2);
			} catch (SlickException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(level==3)
		{
			platforms1.clear();
			platforms2.clear();
			platforms1.add(new Platform1(0, 100, 150, 15));
			platforms1.add(new Platform1(300, 250, 150, 15));
			platforms1.add(new Platform1(1350, 250, 150, 15));
			platforms1.add(new Platform1(1100, 400, 150, 15));
			platforms1.add(new Platform1(500, 500, 150, 15));
			platforms1.add(new Platform1(300, 640, 200, 15));
			platforms1.add(new Platform1(850, 640, 150, 15));
			platforms2.add(new Platform2(850, 640, 15, 300));
			platforms2.add(new Platform2(500, 500, 15, 600));
			
			key.clear();
			try
			{
				key.add(new Key(1400, 160));
				key.add(new Key(700, 285));
				key.add(new Key(100, 410));
				key.add(new Key(600, 700));
				key.add(new Key(900, 700));
				
				dog.setLevel(3);
				burger.setLevel(3);
				doctorFetus.setLevel(3);
			} catch (SlickException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(level==4)
		{
			platforms1.clear();
			platforms2.clear();
			platforms1.add(new Platform1(0, 100, 150, 15));
			platforms1.add(new Platform1(850, 150, 150, 15));
			platforms1.add(new Platform1(850, 300, 350, 15));
			platforms1.add(new Platform1(1300, 425, 200, 15));
			platforms1.add(new Platform1(1000, 550, 200, 15));
			platforms1.add(new Platform1(1300, 650, 200, 15));
			platforms1.add(new Platform1(300, 300, 150, 15));
			platforms1.add(new Platform1(500, 550, 150, 15));
			platforms2.add(new Platform2(1000, 150, 15, 650));
			
			key.clear();
			try
			{
				key.add(new Key(600, 110));
				key.add(new Key(900, 220));
				key.add(new Key(350, 210));
				key.add(new Key(900, 700));
				key.add(new Key(1400, 700));
				
				dog.setLevel(4);
				burger.setLevel(4);
				doctorFetus.setLevel(4);
			} catch (SlickException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(level==5)
		{
			platforms1.clear();
			platforms2.clear();
			platforms1.add(new Platform1(0, 100, 150, 15));
			platforms1.add(new Platform1(300, 250, 150, 15));
			platforms1.add(new Platform1(1100, 250, 150, 15));
			platforms1.add(new Platform1(1350, 400, 150, 15));
			platforms1.add(new Platform1(525, 500, 150, 15));
			platforms1.add(new Platform1(300, 640, 150, 15));
			platforms1.add(new Platform1(900, 640, 150, 15));
			platforms2.add(new Platform2(1050, 640, 15, 300));
			platforms2.add(new Platform2(600, 500, 15, 600));
			
			key.clear();
			try
			{
				key.add(new Key(600, 110));
				key.add(new Key(1450, 360));
				key.add(new Key(100, 410));
				key.add(new Key(400, 700));
				key.add(new Key(1000, 700));
				
				dog.setLevel(5);
				burger.setLevel(5);
				doctorFetus.setLevel(5);
			} catch (SlickException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(level==6)
		{
			platforms1.clear();
			platforms2.clear();
			platforms1.add(new Platform1(0, 100, 150, 15));
			platforms1.add(new Platform1(1350, 200, 150, 15));
			platforms1.add(new Platform1(700, 200, 150, 15));
			platforms1.add(new Platform1(0, 250, 150, 15));
			platforms1.add(new Platform1(700, 400, 150, 15));
			platforms1.add(new Platform1(300, 450, 150, 15));
			platforms1.add(new Platform1(1100, 550, 150, 15));
			platforms1.add(new Platform1(500, 640, 150, 15));
			platforms2.add(new Platform2(745, 400, 15, 600));
			platforms2.add(new Platform2(795, 400, 15, 600));
			
			key.clear();
			try
			{
				key.add(new Key(750, 110));
				key.add(new Key(1400, 110));
				key.add(new Key(50, 210));
				key.add(new Key(200, 700));
				key.add(new Key(1400, 700));
				
				dog.setLevel(6);
				burger.setLevel(6);
				doctorFetus.setLevel(6);
			} catch (SlickException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(level==7)
		{
			platforms1.clear();
			platforms2.clear();
			platforms1.add(new Platform1(0, 100, 150, 15));
			platforms1.add(new Platform1(700, 200, 150, 15));
			platforms1.add(new Platform1(1350, 200, 150, 15));
			platforms1.add(new Platform1(1200, 300, 100, 15));
			platforms1.add(new Platform1(350, 400, 150, 15));
			platforms1.add(new Platform1(900, 450, 150, 15));
			platforms1.add(new Platform1(0, 600, 150, 15));
			platforms1.add(new Platform1(700, 600, 150, 15));
			platforms2.add(new Platform2(900, 350, 15, 100));
			platforms2.add(new Platform2(700, 600, 15, 300));
			platforms2.add(new Platform2(1000, 700, 15, 100));
			
			key.clear();
			try
			{
				key.add(new Key(400, 110));
				key.add(new Key(1400, 110));
				key.add(new Key(1200, 410));
				key.add(new Key(50, 700));
				key.add(new Key(750, 700));
				
				dog.setLevel(7);
				burger.setLevel(7);
				doctorFetus.setLevel(7);
			} catch (SlickException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(level==8)
		{
			platforms1.clear();
			platforms2.clear();
			platforms1.add(new Platform1(0, 100, 150, 15));
			platforms1.add(new Platform1(800, 200, 150, 15));
			platforms1.add(new Platform1(950, 300, 150, 15));
			platforms1.add(new Platform1(200, 300, 150, 15));
			platforms1.add(new Platform1(500, 400, 150, 15));
			platforms1.add(new Platform1(0, 600, 600, 15));
			platforms1.add(new Platform1(800, 600, 600, 15));
			platforms2.add(new Platform2(950, 0, 15, 300));
			
			key.clear();
			try
			{
				key.add(new Key(850, 110));
				key.add(new Key(1400, 110));
				key.add(new Key(1000, 210));
				key.add(new Key(50, 700));
				key.add(new Key(1400, 700));
				
				dog.setLevel(8);
				burger.setLevel(8);
				doctorFetus.setLevel(8);
			} catch (SlickException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(level==9)
		{
			platforms1.clear();
			platforms2.clear();
			platforms1.add(new Platform1(0, 100, 150, 15));
			platforms1.add(new Platform1(300, 100, 150, 15));
			platforms1.add(new Platform1(0, 300, 300, 15));
			platforms1.add(new Platform1(600, 300, 150, 15));
			platforms1.add(new Platform1(1100, 300, 150, 15));
			platforms1.add(new Platform1(350, 400, 150, 15));
			platforms1.add(new Platform1(850, 400, 150, 15));
			platforms1.add(new Platform1(100, 450, 150, 15));
			platforms1.add(new Platform1(1100, 470, 300, 15));
			platforms1.add(new Platform1(400, 500, 150, 15));
			platforms1.add(new Platform1(150, 550, 150, 15));
			platforms1.add(new Platform1(950, 550, 150, 15));
			platforms1.add(new Platform1(0, 620, 150, 15));
			platforms1.add(new Platform1(1100, 630, 150, 15));
			platforms1.add(new Platform1(1350, 630, 150, 15));
			platforms2.add(new Platform2(1100, 470, 15, 300));
			platforms2.add(new Platform2(1250, 630, 15, 150));
			platforms2.add(new Platform2(1350, 630, 15, 150));
			
			key.clear();
			try
			{
				key.add(new Key(50, 210));
				key.add(new Key(1150, 210));
				key.add(new Key(25, 560));
				key.add(new Key(1150, 560));
				key.add(new Key(1000, 700));
				
				dog.setLevel(9);
				burger.setLevel(9);
				doctorFetus.setLevel(9);
			} catch (SlickException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(level==10)
		{
			platforms1.clear();
			platforms2.clear();
			platforms1.add(new Platform1(0, 100, 600, 15));
			platforms1.add(new Platform1(700, 100, 300, 15));
			platforms1.add(new Platform1(100, 250, 400, 15));
			platforms1.add(new Platform1(1350, 250, 150, 15));
			platforms1.add(new Platform1(300, 400, 450, 15));
			platforms1.add(new Platform1(900, 400, 200, 15));
			platforms1.add(new Platform1(1350, 400, 150, 15));
			platforms1.add(new Platform1(600, 500, 300, 15));
			platforms1.add(new Platform1(1350, 640, 1150, 15));
			platforms1.add(new Platform1(100, 640, 1150, 15));
			platforms2.add(new Platform2(100, 250, 15, 390));
			platforms2.add(new Platform2(600, 100, 15, 150));
			platforms2.add(new Platform2(700, 100, 15, 150));
			platforms2.add(new Platform2(1150, 0, 15, 500));
			platforms2.add(new Platform2(1250, 200, 15, 455));
			
			key.clear();
			try
			{
				key.add(new Key(1350, 60));
				key.add(new Key(1050, 110));
				key.add(new Key(800, 160));
				key.add(new Key(25, 410));
				key.add(new Key(200, 460));
				
				dog.setLevel(10);
				burger.setLevel(10);
				doctorFetus.setLevel(10);
			} catch (SlickException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	public static void main(String[] argv)
	{
		try
		{
			AppGameContainer container = new AppGameContainer(new Main());
			container.setDisplayMode(1500, 900, false); //1500, 900 //ToD 1920, 1080
			container.setShowFPS(false);
			container.start();
		} 
		catch (SlickException e)
		{
			e.printStackTrace();
		}
	}
}
