package superMeatBoy;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;

import java.util.ArrayList;
import java.util.List;

public class Meatboy
{
	private double force = 10;
	private boolean moveleft = false;
	private boolean moveright = false;
	private boolean jumping = false;
	private boolean death = false;
	public boolean nextLevel = false;
	private Image meatboyImage;
	private double ySpeed = 0;
	private boolean allowJump = true;
	
	private Rectangle meatboy;
	
	public Meatboy(int x, int y) throws SlickException
	{
		super();
		meatboy = new Rectangle(x, y, 42, 36);

		meatboyImage = new Image("testdata/supermeatboy/meatboy/front.png");
	}

	public void update(GameContainer container, int delta, List<Platform1> platforms1, List<Platform2> platforms2, List<Enemy> enemies, List<Key> keys, KeyCounter counter, Door door, boolean restart) throws SlickException
	{
		if(restart == true)
		{
			meatboy.setX(75);
			meatboy.setY(725);
			death = false;
		}
		
		if(death == true)
		{
			return;
		}

		moveleft = container.getInput().isKeyDown(Input.KEY_LEFT);
		moveright = container.getInput().isKeyDown(Input.KEY_RIGHT);
		jumping =  container.getInput().isKeyDown(Input.KEY_UP);
		
		force -= 0.05;
		
		boolean jumped = false;	

		if (moveleft == true)
		{	
			if(meatboyImage.getName() != "left" && moveright == false)
			{
				meatboyImage.destroy();
				meatboyImage = new Image("testdata/supermeatboy/meatboy/left.png");
				meatboyImage.setName("left");
			}
			meatboy.setX(meatboy.getX()-1);

			if (meatboy.getX() <= 0)
			{
				meatboy.setX(meatboy.getX()+1);
			}
		}
		if (moveright == true)
		{
			if(meatboyImage.getName() != "right" && moveleft == false)
			{
				meatboyImage.destroy();
				meatboyImage = new Image("testdata/supermeatboy/meatboy/right.png");
				meatboyImage.setName("right");
			}

			meatboy.setX(meatboy.getX()+1);
			if (meatboy.getX() >= 1500-meatboy.getWidth())
			{
				meatboy.setX(meatboy.getX()-1);
			}
		}
		
		boolean sidecollision = false;
		
		for(Platform2 platform : platforms2)
		{
			if(meatboy.intersects(platform.platform))
			{
				sidecollision = true;
			}		
		}
		
		for(Platform1 platform : platforms1)
		{
			if(meatboy.intersects(platform.platform))
			{
				sidecollision = true;
			}		
		}
		
		if(sidecollision == true)
		{
			if(moveright == true)
			{
				meatboy.setX(meatboy.getX()-1);
			}
			if(moveleft == true)
			{
				meatboy.setX(meatboy.getX()+1);
			}
			sidecollision = false;
		}
		
		if(((moveright == false && moveleft == false) | (moveright && moveleft)) && meatboyImage.getName() != "straight")
		{
			meatboyImage.destroy();
			meatboyImage = new Image("testdata/supermeatboy/meatboy/front.png");
			meatboyImage.setName("straight");
		}
		
		ySpeed = ySpeed + 0.1f;

		if (jumping == true && allowJump)
		{
			ySpeed = -10f;
			jumped = true;
			jumping = false;
			allowJump = false;
		}

		//this.y -= force;
		//meatboy.setY(meatboy.getY() - (float)force);
		meatboy.setY(meatboy.getY() + (float) ySpeed);


		if (meatboy.getY() >= 725)
		{
			meatboy.setY(725);
			jumping = false;
			ySpeed = 0;
			allowJump = true;
		}
		
		if (meatboy.getY() <= 0)
		{
			meatboy.setY(0);
			jumping = false;
			ySpeed = 0;
		}
		
		boolean topcollision = false;
		
		for(Enemy enemy : enemies)
		{
			if(meatboy.intersects(enemy.getRectangle()))
			{
				sidecollision = true;
				meatboyImage = new Image("testdata/supermeatboy/meatboy/dead.png");
				death = true;
			}
		}
		
		Key temp = null;
		for(Key key : keys)
		{
			if(meatboy.intersects(key.key))
			{
				sidecollision = true;
				temp = key;
				counter.count ++;
				System.out.println("keys: " + counter.count);
			}
		}
		if(temp != null)
		{
			keys.remove(temp);
		}
		if(meatboy.intersects(door.door))
		{
			nextLevel = true;
		} else
		{
			nextLevel = false;
		}
		
		for(Platform1 platform : platforms1)
		{
			if(meatboy.intersects(platform.platform))
			{
				topcollision = true;
				if(ySpeed > 0)allowJump = true;
				meatboy.setY(meatboy.getY() - (float)ySpeed);
				ySpeed = 0;
			}			
		}
		
		for(Platform2 platform : platforms2)
		{
			if(meatboy.intersects(platform.platform))
			{
				topcollision = true;
				if(ySpeed > 0)allowJump = true;
				meatboy.setY(meatboy.getY() - (float)ySpeed);
				ySpeed = 0;
			}			
		}
	
		if(topcollision == true)
		{			
			if(jumped == true)
			{
				meatboy.setY(meatboy.getY()-(float)ySpeed);
			}
		}
		
		if(topcollision && !jumping)
		{
			meatboy.setY(meatboy.getY() - (float)ySpeed);
		}
		
		for(Platform1 platform : platforms1)
		{
			if(meatboy.intersects(platform.platform))
			{
				topcollision = true;
				meatboy.setY(meatboy.getY() - (float)ySpeed);
				ySpeed = 0;
			}			
		}
		
		for(Platform2 platform : platforms2)
		{
			if(meatboy.intersects(platform.platform))
			{
				topcollision = true;
				meatboy.setY(meatboy.getY() - (float)ySpeed);
				ySpeed = 0;
			}			
		}
	}

	public void render(Graphics g)
	{
		meatboyImage.draw((float) meatboy.getX(), (float) meatboy.getY(), meatboy.getWidth(), meatboy.getHeight());
	}
}