package superMeatBoy;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;

public class Platform2
{
	public Rectangle platform;

	public Platform2(int x, int y, int width, int height)
	{
		super();
		platform = new Rectangle(x, y, width, height);
	}

	public void update(int delta)
	{

	}

	public void render(Graphics g)
	{
		g.setColor(Color.black);
		g.fillRect(platform.getX(), platform.getY(), platform.getWidth(), platform.getHeight());
	}
}